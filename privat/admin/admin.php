<!DOCTYPE html>
<?php
  include ("../BD/funcionsBD.php");
  include ("../llibreries/funcions.php");
  if($_POST)
    $dia = str_replace('-', '',$_POST['dia']);
  else
    $dia = date("Ymd");
  $reserves = reserves_dia($dia);
  $timetable = imprimeixAdmin($dia, $reserves);
?>

<html lang="ca">
<head>
  <?php include("metadataAdmin.php") ?>
  <script type="text/javascript" src="../llibreries/libreria_jquery.js"></script>
</head>
<body>
  <?php include("headerAdmin.php") ?>
  <div class="container">
    <h1 class="site-title">Cita prèvia ITV</h1>
    <div class="row">
      <div class="column">
        <article class="admin_article">
          <h2>Cites Programades per avui: </h2>
          <form class="form iform" action="admin.php" method="post">
            <input class="dia_admin" type="date" name="dia" placeholder="YYYY-MM-DD" pattern="[0-9]{4}-(0[1-9]|1[012])-([012][0-9]|3[01])" required autofocus>
            <input class="button" type="submit" value="Enviar">
          </form>
          <?php echo $timetable; ?>
        </article>
      </div>
    </div>
  </div>
  <?php include("../../public/html/footer.php") ?>
  </body>
</html>