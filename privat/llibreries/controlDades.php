<?php
	include ("funcions.php");
	/**Fitxer de funcions que validen el si les dades estan o no en el format correcte.*/

	/**Comprova que el dia té un format correcte i és correcte.
	* @param $data Dia a comprovar (YYYMMDD).
	* @return Boleà que indica si el dia és o no correcte.
	*/
	function comprovaDia($data)
	{
		$data = testInput($data);
		$any = intval(substr($data, 0, -4));
		$mes = intval(substr($data, 4, -2));
		$dia = intval(substr($data, -2, 2));
		//L'any només pot ser l'any actual o el següent la que només permetem reservar a 4 mesos vista.
		if(!($any==date('Y') || $any==date('Y')+1))
			return false;
		//Si l'any és l'actual el mes no pot ser inferior a l'actual.
		if(!($mes<13 && ($any==date('Y') && $mes>=date('m') || $any==date('Y')+1 && $mes>0)))
			return false;
		//
		if(!($dia<=diesMes($mes, $any) && ($mes!=date('m') && $dia>0 || $mes==date('m') && $dia>=date('d'))))
			return false;
		return true;
	} 

	/**Comprova si l'hora introduida esta en el format correcte o no amb l'ajuda de les regex.
	* @param $hora Hora a comprovar (HH:MM).
	* @return Boleà que indica si el format de l'hora és o no correcte.
	*/
	function comprovaHora($hora)
	{
		$aux = "/(0[89]|1[0-9]):(3|0)0:00/";
		return (bool)preg_match($aux, $hora);
	}

	/**Passa l'string per testInput i agafa només els 4 primers i els tres últims caràcters passats a majúscula.
	* @param $matricula matricula a arreglar
	* @return String amb la matrícula arreglada.
	*/
	function arreglaMat($matricula)
	{
		$matricula = testInput($matricula);
		$matricula = substr($matricula, 0 ,4) . strtoupper(substr($matricula, -3, 3));
		return $matricula;
	}

	/**Comprova la matrícula té un format correcte amb ajuda d les regex.
	* @param $matricula Matricula a comprovar.
	* @return Boleà que indica si el format de la matricula és o no correcte.
	*/
	function comprovaMat($matricula)
	{
		$aux = "/([0-9]{4})([B-DF-HJ-NP-TV-Z]{3})/";
		return (bool)preg_match($aux, $matricula);
	}

	/**Comprova que el numero de telefon no contingui caràcters que no siguin numèrics i que sigui de 9 dígits.
	* @param $tlf String amb el número de telèfon a comprovar.
	* @return Boleà que indica si el format del telèfon és o no correcte.
	*/
	function comprovaTlf($tlf)
	{
		$tlf = testInput($tlf);
		$aux = "/[0-9]{9}/";
		return (bool)preg_match($aux, $tlf);
	}

	/**Comprova que l'string de nom i cognoms no superi els 30 caràcters i que no tingui caràcters extranys (es permeten accents en cas contrari hauriem fet servir les regex).
	* @param $nom String amb el nom i cognoms a comprovar.
	* @return Boleà que indica si el format del nom és o no correcte.
	*/
	function comprovaNom($nom)
	{
		$nom = testInput($nom);
		$nom = strtolower($nom);
		if(strlen($nom)>30)
			return false;
		for($i=0;$i<strlen($nom);$i++)
			if(!ctype_space($nom[$i]) && ctype_punct($nom[$i]))
				return false;
		return true;
	}

	/**Comprova que l'email és vàlid amb ajuda de la funció filter_var que valida emails amb la sintaxi RFC 822, sense admetre espais en blanc. 
	* @param $email String amb l'email a validar.
	* @return Boleà que indica si el format de l'email és o no correcte.
	*/
	function comprovaEmail($email)
	{
		if(filter_var($email, FILTER_VALIDATE_EMAIL))
			return true;
		return false;
	}
?>