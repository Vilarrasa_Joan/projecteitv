<?php
	/**Elimina els espais en blanc al final i principi de l'input, elimina els caràcters de escapament i canvia els caràcters especials de html.
	* @param $input Dades introduides a arreglar.
	* @return Dades arreglades.
	*/
	function testInput($input)
	{
		$input = trim($input);
		$input = stripslashes($input);
		$input = htmlspecialchars($input);
		return $input;
	}

	/**Comprova si tots els dígits de un string són numèrics i si no hi ha decimals.
	* @return Boleà que indica si el número és o no enter.
	*/
	function esEnter($numero)
	{
		for($i=0;$i<strlen($numero);$i++)
			if(!is_numeric($numero[$i]))
				return false;
		return true;
	}

	/**Funció que retorna els dies de un mes concret
	* @param $mes Número del mes
	* @param $any Any del mes
	* @return Integer que indica quants dies té el mes concret.
	*/
	function diesMes($mes, $any)
	{
		return $mes == 2 ? ($any % 4 ? 28 : ($any % 100 ? 29 : ($any % 400 ? 28 : 29))) : (($mes - 1) % 7 % 2 ? 30 : 31);
	}

	/**Funció que extreu una taula calendari amb els dies ocupats de un mes concret segons la BD.
	* @param $primerDia Número de dia de la setmana del dia 1 del mes.
	* @param $diesMes Número de dies que té el mes
	* @param $diesOcupats Array amb el dies ocupats del mes.
	* @param $mes Número del mes.
	* @param $any Número de l'any.
	* @return String amb una taula html que representa un calendari amb els dies ocupats.
	*/
	function imprimeixCalendari($primerDia, $diesMes, $diesOcupats, $mes, $any)
	{
		$dia_actual=date('d');
		$mes_actual=date('m');
		$mesos = ['Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre', 'Octubre', 'Novembre', 'Desembre'];
		//Guardem en una variable el nom de mes.
		$titol = $mesos[intval($mes)-1];
		//Capçalera edl calendari amb els dies
		$calendari = "<table><tr><th colspan='4' class='mes'>" . $titol . "</th><th colspan='3' class='any'>" . $any . "</th></tr><tr><th>DU</th><th>DM</th><th>DI</th><th>DJ</th><th>DV</th><th>DS</th><th>DG</th></tr>";
		//La variable d fa referència al dia del mes
		$d = 1;
		//Posició a l'array de dies ocupats de l'ultim dia ocupat afegir calendari.
		//Utilitzarem aquesta variable ja que l'array de dies ocupats està ordenada per data i el calendari
		//s'omple també per data per tant no cal recórrer tot el vector, només des de l'ultim dia ocupat. 
		$pos=0;
		//Hi haurà com a màxim 6 línies
		$fiCalendari = false;
		$i = 0;
		while($i<6 && !$fiCalendari)
		{
			//Per a cada línia obrim el tr i creem 7 elements
			$calendari = $calendari . "<tr>";          
			for($j=0;$j<7;$j++)
			{
				//Utilitzant la posició del primer dia de la setmana podem saber quan hem de comença a omplir el calendari
				//La primera linia no pot començar fins que arribi a la posició del primer dia
				//Les dues últimes línies quan acabem els dies ha de acabar
				if(($i==0 && $j<$primerDia) || ($i==4 | $i==5) && $d>$diesMes)
					$calendari = $calendari . "<td></td>";
				else 
				{
					$k=$pos;
					$ocupat=False;
					//Recórrer l'array de dies ocupats per saber si el dia d està o no ple.
					while($k<count($diesOcupats) && !$ocupat){
						//El format de la consulta és 'YYYY/mm/dd' per tant només hem de mirar els dos ultims digits.
						if(substr($diesOcupats[$k][0], -2)==$d){
							$ocupat=True;
							//Guardem el nou punt de partida per comprovar dies ocupats.
							$pos=$k+1;
						}
						$k++;
					}
					if($j==6) $ocupat=True;
					//En cas que el dia estigui ocupat o que sigui diumenge o que sigui anterior al dia actual li afegirem una classe per la presentació al costat client.
					if($ocupat || ($d<$dia_actual && $mes == $mes_actual))
						$calendari = $calendari . "<td class='ocupat'>" . $d . "</td>";
					else
						$calendari = $calendari . "<td class='lliure'>" . $d . "</td>";
					$d++;
				}
			}
			$calendari = $calendari . "</tr>";
			$i++;
			if($d>$diesMes) $fiCalendari = true;
		}
		$calendari = $calendari . "</table>";
		return $calendari;
	}

	/**Funció imprimeix un horari de 08:00 a 19:30 amb salts de 30 min i indica quines hores estan ocupades.
	* @param $horesOcupades Array amb les hores que estan ocupades.
	* @return String amb una taula html que representa un horari amb les hores ocupades.
	*/
	function imprimeixHorari($horesOcupades)
	{
		//Capçalera de l'horari
		$horari = "<table><tr><th colspan='4'>Horari</th><tr>";
		$hora = 8;
		//Farem servir una variable per canviar la hora cada dues iteracions, a la primera amb 00 i a la segona amb 30.
		$par=0;
		//Guardarem la posició de l'ultima hora ocupada per no haver de recórrer tot l'array cada cop
		$pos=0;
		for($i=0;$i<6;$i++)
		{
			$horari = $horari . "<tr>";
			for($j=0;$j<4;$j++)
			{
				if($par==0)	{
					$aux = sprintf("%02d", $hora) . '00'; 
					$par=1;
				}
				else {
					$aux = sprintf("%02d", $hora) . '30';
					$hora++;
					$par=0;
				}
				$l=$pos;
				$ocupat=False;
				//Recórrer l'array de hores ocupades per saber si el dia d està o no ple.
				while($l<count($horesOcupades) && !$ocupat){
					if(strcmp($aux, $horesOcupades[$l][0])==0){
						$ocupat=True;
						$pos=$l;
					}
					$l++;
				}
				//Afegim els ':' a les hores. 
				$aux = substr_replace($aux, ':',2,0);
				if($ocupat) 
					$horari = $horari . "<td class='ocupat'>" . $aux . "</td>";
				else
					$horari = $horari . "<td class='lliure'>" . $aux . "</td>";
			}
			$horari = $horari . "</tr>";
		}
		$horari = $horari . "</table>";
		return $horari;
	}

	/**Funció imprimeix un horari de 08:00 a 19:30 amb salts de 30 min i indica quines linies i per quin vehicle estan ocupades.
	* @param $dia Dia de l'horari (YYYYMMDD).
	* @return String amb una taula html que representa un horari amb les hores ocupades i per qui estan ocupades.
	*/
	function imprimeixAdmin($dia, $reserves){
    	$mesos = ['Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre', 'Octubre', 'Novembre', 'Desembre'];
    	$titol = $mesos[intval(substr($dia, 4, 2))-1];
    	//Capçalera del calendari amb els dies
    	$horari = "<table><tr><th colspan='7'>" . substr($dia, -2, 2) . " de " . $titol . " del " . substr($dia, 0, 4) . "</th></tr><tr><th>Hora</th><th colspan='3'>Línia 1</th><th colspan='3'>Línia 2</th></tr>";
    	$hora = 8;
    	//Guardarem la posició de l'ultima hora ocupada per no haver de recórrer tot l'array cada cop
    	$pos=0;
    	$hores = ['08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30'];
    	for($i=0;$i<count($hores);$i++)
    	{
    	  $horari = $horari . "<tr><td>" . $hores[$i] . "</td>";
    	  $j = $pos;
    	  $trobat = false;
    	  while($j<count($reserves) && !$trobat)
    	  {
    	    if(strcmp($reserves[$j][0], $hores[$i])==0)
    	    {
    	      $trobat=true;
    	      $horari = $horari . "<td>" . $reserves[$j][2] . "</td><td>" . $reserves[$j][3] . "</td><td>" . $reserves[$j][4] . "</td>";
    	      $pos = $j;
    	      if(strcmp($reserves[$j+1][0], $hores[$i])==0)
    	      {
    	        $horari = $horari . "<td>" . $reserves[$j+1][2] . "</td><td>" . $reserves[$j+1][3] . "</td><td>" . $reserves[$j+1][4] . "</td>";
    	        $pos = $j+1;
    	      }
    	      else
    	      {
    	        $horari = $horari . "<td></td><td></td><td></td>";
    	      }
    	    }
    	    $j++;
    	  }
    	  $horari = $horari . "</tr>";
    	}
    	$horari = $horari . "</table>";
    	return $horari;
  	}
?>