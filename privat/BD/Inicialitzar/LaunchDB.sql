/*use ITV;*/
use a16davmorgim_ITV;

create table if not exists VEHICLE (
matricula varchar(7) primary key,
nom varchar(30),
tlf int(9),
email varchar(30),
tipus varchar(15) default 'turisme'
);

create table if not exists TALLER (
id int auto_increment primary key,
nom varchar(30)
);

create table if not exists LINIA (
num tinyint unsigned auto_increment,
id_taller int,
tipus varchar(15) default 'turisme',
primary key (num, id_taller),
foreign key (id_taller) references TALLER(id) on delete cascade on update cascade
);

create table if not exists RESERVA (
matricula varchar(7),
num_linia tinyint unsigned,
id_taller int,
hora datetime,
primary key (matricula, num_linia, id_taller, hora),
foreign key (id_taller) references LINIA(id_taller) on delete cascade on update cascade,
foreign key (num_linia) references LINIA(num) on delete cascade on update cascade,
foreign key (matricula) references VEHICLE(matricula) on delete cascade on update cascade
);

create table if not exists RESERVAT (dia date primary key);

#Procediment que inserta una reserva de una matricula en una hora determinada.
delimiter //
create procedure insertar_reserva (in mat varchar(7), in hor datetime, out insertat boolean)
begin
	declare num_reserves int default 0;
	#Comprovar que la hora de la reserva no estigui plena.
    set num_reserves = (select count(*) from RESERVA where RESERVA.hora=hor);
    start transaction;
    	#si està plena retornem un false i el procediment retornarà false.
		if num_reserves = 2
		then
			set insertat=false;
		#si només hi ha una reserva l'insertem a la segona linia
		elseif num_reserves = 1
		then
			insert into RESERVA values (mat, 2, 1, hor);
			set insertat=true;
		#si no hi han reserves l'insertem a la primera linia
		else
			insert into RESERVA values (mat, 1, 1, hor);
			set insertat=true;
		end if;
		#si hi ha més de dos reserves després de insertar fem un rollback i cancelem la operació.
        if (select count(*) from RESERVA where RESERVA.hora=hor) > 2
         then
			rollback;
            set insertat=false;
		end if;
	commit;
	#si hem aconseguit insertar la reserva mirem si la mateixa que que el dia quedi ple. En cas afirmatiu insertem la data del dia a la taula de dies plens.
    if insertat=true
     then 
		if (select count(*) from RESERVA where date_format(hora, '%Y%m%d')=date_format(hor, '%Y%m%d')) >= 48
		 then
			insert into RESERVAT values (str_to_date(date_format(hor, '%Y%m%d'), '%Y%m%d'));
		end if;
	end if;
end //
delimiter ;

#Procediment que inserta un vehicle a la BD.
delimiter //
create procedure insertar_vehicle(in matricula varchar(7), in nom varchar(30),in tlf int(9),in email varchar(30), out insertat boolean)
begin
	set insertat = true;
	#Comprovem que no hi hagi un altre vehicle amb la mateixa matrícula.
	if (select count(*) from VEHICLE where VEHICLE.matricula=matricula) >= 1
     then
		set insertat=false;
	#abans de insertar comprovem que el format de la matrícula sigui correcte.
	 else
		if (matricula regexp '([0-9]{4})([B-DF-HJ-NP-TV-Z]{3})') = 0
         then
			set insertat=false;
		end if;
	end if;
	#Si les comprovacions són correctes insertem el vehicle a la taula.
    if insertat = true
     then
		insert into VEHICLE values (matricula, nom, tlf, email, default);
	end if;
end //
delimiter ;

#Funció que retorna un boleà segons si hi ha una reserva activa per a una matrícula concreta o no.
delimiter //
create function reserva_activa(matricula varchar(7))
returns boolean
begin
	if (select count(*) from RESERVA where RESERVA.matricula=matricula and hora>now()) >= 1
     then
		return true;
	end if;
    return false;
end //
delimiter ;

insert into TALLER set nom='taller1';
insert into LINIA set id_taller=1;
insert into LINIA set id_taller=1;
insert into VEHICLE values ('1111AAA', 'Pedro', 111222333, 'pedraso@coldmail.com', default);
insert into VEHICLE values ('2222BBB', 'Paco', 444555666, 'pacaso@coldmail.com', default);
insert into VEHICLE values ('3333CCC', 'Pablo', 777888999, 'pablaso@coldmail.com', default);
insert into VEHICLE values ('4444EEE', 'Pol', 333222111, 'polaso@coldmail.com', default);

call insertar_reserva('1111AAA', str_to_date('20171225 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171225 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171227 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171230 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20171231 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180103 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180105 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180106 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 0900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1100','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1400','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1600','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1730','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('1111AAA', str_to_date('20180107 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171228 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20171229 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180102 1930','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 0800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 0830','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 0930','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1000','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1030','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1130','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1200','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1230','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1300','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1330','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1430','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1500','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1530','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1630','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1700','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1800','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1830','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1900','%Y%m%d %H%i'), @ok);
call insertar_reserva('3333CCC', str_to_date('20180108 1930','%Y%m%d %H%i'), @ok);
