<?php
	/**Extreure les dades de una reserva amb matricula concreta i hora en el futur, és a dir, la reserva activa de un veicle concret.
	* @param $matricula Matricula de la reserva a buscar.
	* @return Array amb les dades del vehicle i de la reserva.
	*/
	function extreu_dades_reserva($matricula)
	{
		require 'login.php';
		$query = "select nom,tlf,email,hora from RESERVA left outer join VEHICLE on RESERVA.matricula=VEHICLE.matricula where RESERVA.matricula='" . $matricula . "' and hora>now()";
		$result = mysqli_query($db_server, $query);
		if (!$result) 
			die ("Database access failed: " . mysql_error());
		$dades = mysqli_fetch_all($result);
		return $dades;
	}
	/**Extreuer dies ocupats la llista dels dels dies ocupats de un mes concret ordenada per dia.
	* @param $any Any del mes a buscar dies ocupats.
	* @param $mes Mes a buscar dies ocupats.
	* @return Array amb els dies ocupats del mes.
	*/
	function llista_dies_ocupats($any, $mes)
	{
		require 'login.php';
		$query = "select * from RESERVAT where date_format(dia, '%Y%m')='" . $any . $mes . "'";
		$result = mysqli_query($db_server, $query);
		if (!$result) 
			die ("Database access failed: " . mysql_error());
		$ocupats = mysqli_fetch_all($result);
		return $ocupats;
	}

	/**Extreuer les hores ocupades de un dia concret ordenades per hora.
	* @param $any Any del dia a buscar les hores ocupades.
	* @param $mes Mes del dia a buscar les hores ocupades.
	* @param $dia Dia a buscar les hores ocupades.
	* @return Array amb les hores ocupades del dia.
	*/
	function llista_hores_ocupades($any, $mes, $dia)
	{
		require 'login.php';
		$query = "select date_format(hora, '%H%i') from RESERVA where date_format(hora, '%Y%m%d')='" . $any . $mes . $dia . "' group by date_format(hora, '%H%i') having count(*)=2 order by date_format(hora, '%H%i');";
		$result = mysqli_query($db_server, $query);
		if (!$result) 
			die ("Database access failed: " . mysql_error());
		$ocupats = mysqli_fetch_all($result);
		return $ocupats;
	}

	/**Comprova si un vehicle té reserva.
	* @param $matricula Matricula del vehicle.
	* @return Boleà que indica si una matrícula té o no una reserva activa.
	*/
	function te_reserva($matricula)
	{
		require 'login.php';
		$query = "select matricula from RESERVA where RESERVA.matricula='" . $matricula . "' and hora>now()";
		$result = mysqli_query($db_server, $query);
		if (!$result) 
			die ("Database access failed: " . mysql_error());
		$reserva = mysqli_fetch_all($result);
		if($reserva)
			return true;
		return false;
	}

	/**Comprova si un vehicle està registrat a la BD.
	* @param $matricula Matricula del vehicle.
	* @return Boleà que indica si el vehicle està registrat o no.
	*/
	function vehicle_registrat($matricula)
	{
		require 'login.php';
		$query = "select * from VEHICLE where matricula='" . $matricula . "'";
		$result = mysqli_query($db_server, $query);
		if (!$result) 
			die ("Database access failed: " . mysql_error());
		$registre = mysqli_fetch_all($result);
		if($registre) 
			return true;
		return false;
	}
	
	/**Extreure les dades de un vehicle.
	* @param $matricula Matricula del vehicle.
	* @return Array amb les dades del vehicle.
	*/
	function dades_vehicle_registrat($matricula)
	{
		require 'login.php';
		$query = "select * from VEHICLE where matricula='" . $matricula . "'";
		$result = mysqli_query($db_server, $query);
		if (!$result) 
			die ("Database access failed: " . mysql_error());
		$registre = mysqli_fetch_all($result);
		return $registre;
	}

	/**Inserta un vehicle a la base de dades.
	* @param $matricula Matricula del vehicle.
	* @param $nom Nom del propietari del vehicle.
	* @param $tlf Telèfon del propietari del vehicle.
	* @param $email Email del propietari del vehicle.
	* @return Boleà que retorna true si la inserció s'ha executat amb èxit.
	*/
	function insertar_vehicle($matricula, $nom, $tlf, $email)
	{
		require 'login.php';
		$query = "call insertar_vehicle('" . $matricula . "', '" . $nom . "', '" . $tlf . "', '" . $email . "', @insertat);";
		$result = mysqli_query($db_server, $query);
		if (!$result) 
			die ("Database access failed: " . mysql_error());
		$insertat = mysqli_query($db_server, "select @insertat");
		$correcte = mysqli_fetch_all($insertat);
		if($correcte[0][0]==1)
			return true;
		return false;
	}

	/**Actualitza les dades de un vehicle registrat a la BD.
	* @param $matricula Matricula del vehicle.
	* @param $nom Nom del propietari del vehicle.
	* @param $tlf Telèfon del propietari del vehicle.
	* @param $email Email del propietari del vehicle.
	* @return Boleà que retorna true si la actualització s'ha executat amb èxit.
	*/
	function actualitzar_vehicle($matricula, $nom, $tlf, $email)
	{
		require 'login.php';
		$query = "update VEHICLE set nom='" . $nom . "', tlf='" . $tlf . "', email='" . $email . "' where matricula='" . $matricula . "';";
		$result = mysqli_query($db_server, $query);
		if (!$result) 
			die ("Database access failed: " . mysql_error());
		return true;
	}

	/**Inserta una reserva nova a la BD.
	* @param $matricula Matricula del vehicle.
	* @param $hora Hora de la reserva.
	* @return Boleà que retorna true si la inserció s'ha executat amb èxit.
	*/
	function insertar_reserva($matricula, $hora)
	{
		require 'login.php';
		$query = "call insertar_reserva ('" . $matricula . "','" . $hora . "', @insertat)";
		$result = mysqli_query($db_server, $query);
		if (!$result) 
			die ("Database access failed: " . mysql_error());
		$insertat = mysqli_query($db_server, "select @insertat");
		$correcte = mysqli_fetch_all($insertat);
		if($correcte[0][0]==1)
			return true;
		return false;
	}

	/**Actualitza l'hora d'una reserva de la BD. És realment la creació de una reserva nova i l'eliminaciño de la reserva antiga.
	* @param $matricula Matricula del vehicle.
	* @param $hora Hora nova de la reserva.
	* @return Boleà que retorna true si la actualització s'ha executat amb èxit.
	*/
	function actualitzar_reserva($matricula, $hora)
	{
		require 'login.php';
		$query = "call insertar_reserva ('" . $matricula . "','" . $hora . "', @insertat)";
		$result = mysqli_query($db_server, $query);
		if (!$result) 
			die ("Database access failed: " . mysql_error());
		$insertat = mysqli_query($db_server, "select @insertat");
		$correcte = mysqli_fetch_all($insertat);
		//En cas que s'hagi creat bé la nova reserva podem eliminar l'anterior. En cas contrari no eliminem la reserva anterior.
		if($correcte[0][0]==1)
		{
			$query = "delete from RESERVA where RESERVA.matricula='" . $matricula . "' and hora>now() and hora!='" . $hora . "';";
			$result = mysqli_query($db_server, $query);
			if (!$result) 
				die ("Database access failed: " . mysql_error());
			return true;
		}
		return false;
	}

	/**Esborra una reserva de la BD.
	* @param $matricula Matricula del vehicle.
	* @return Boleà que retorna true si l'esborrat s'ha executat amb èxit.
	*/
	function esborrar_reserva($matricula)
	{
		require 'login.php';
		$query = "delete from RESERVA where RESERVA.matricula='" . $matricula . "' and hora>now();";
		$result = mysqli_query($db_server, $query);
		if (!$result) 
			die ("Database access failed: " . mysql_error());
		return true;
	}

	/**Extreu una llista de reserves i les dades del vehicle que té la reserva.
	* @param $data Dia (YYYYMMDD) d'on extreure les dades.
	* @return Array amb les dades de les hores reservades i les reserves.
	*/
	function reserves_dia($data)
	{
		require 'login.php';
		$query = "select date_format(hora, '%H:%i') as hora, num_linia, RESERVA.matricula, nom, tlf from RESERVA left outer join VEHICLE on RESERVA.matricula=VEHICLE.matricula where date_format(hora, '%Y%m%d')=" . $data . " order by date_format(hora, '%H:%i'), num_linia;";
		$result = mysqli_query($db_server, $query);
		if (!$result) 
			die ("Database access failed: " . mysql_error());
		$ocupades = mysqli_fetch_all($result);
		return $ocupades;
	}
?>
