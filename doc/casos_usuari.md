Cas correcte:

Quan un usuari demana una cita, tria entre dues opcions, demanar cita o modificar cita.
Quan demana cita, se li passa a una pàgina amb un calendari amb els dies disponibles. Un cop escollit un dia, s'accedeix a una pàgina on es mostren les hores disponibles. En clicar a l'hora concreta se li demanarà a l'usuari que introdzca les dades necessàries, on una vegada validat les dades es requerirà una confirmació, i per acabar, se li mostrarà una resposta que tot a sortit correcte.

Quan accedeix a modificar cita, s ele donen tres opcions:
Modificar data, en la qual se li envia a l'calendari on tria un altre dia i data i se li demana confirmació.
Modificar dades, on se li mostren les dades actuals, i se li permet a l'usuari modificar-los, un cop canviats es pid econfirmación.
Esborrar cita, on se li mostra un resum de les dades de la seva cita, amb hora i dades de l'usuari, i es pregunta si vol esborrar la cita, es demana confirmació i s'esborra.

Casos extrems:

En cas de que el dia estigui ocupat, no se li deixarà clicar en aquell dia.
En cas de que una hora concreta ja estigui ocupada, no se li deixarà demanar en aquella hora.
En cas de que una de les dades no estiguin be, se li avisarà a l'usuari avans de deixar-li accedir a la pagina de confirmació. (Es valida en javascript i en php per si l'usuari te desactivat el javascript)
En cas de que hi hagi una error a l'hora de confirmar la cita, s'informara a l'usuari de que la seva cita no s'ha pogut realitzar i se li demanara que ho torni a provar.
En cas de tenir una cita ja demanda per a la mateixa matricula, se l'informarà de la cita ja concretada, informant-lo de que no pot reservar una altre cita fins que no hagi passat la concretada, i que en cas de voler modificarla, accedeixi a la secció de modificació.
