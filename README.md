### What is this repository for? ###

Aplicaci� per gestionar cites per passar la itv.
Version: 0.1

###Autors:

* Adri�n Caballero
* David Moreno
* Joan Vilarrasa

##Prototip:
[Moqup](https://app.moqups.com/a16davmorgim@iam.cat/GlfI6pOXgY/view)

##Seguiment
[Document seguiment projecte transversal](https://docs.google.com/a/iam.cat/spreadsheets/d/1r1WuzJlQzekGBjP5e-2RxiT02ot78javCv5ucQ6ohpQ/edit?usp=sharing)

##Labs

[Link del labs](http://labs.iam.cat/~a16davmorgim/projecteitv/public/html/)