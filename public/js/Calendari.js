$(document).ready(inicializarEventos);

function inicializarEventos(){
  $("table,.i_hide").hide();
  $("table").first().show();
  $(".prev, .next").click(moveMonth);
  $(".lliure").click(clickDia);
  $(".form").submit(function(event){
  	if($(".i_hide").val()==""){
  		alert("Selecciona un dia");
  		event.preventDefault();
  	}
  });
  disableButton();
}

var mes_visible=0;

function clickDia(){
	var dia=$(this).text();
	var mes=$(".mes").eq(mes_visible).text();
	var any=$(".any").eq(mes_visible).text();

	$(".i_hide").eq(0).attr("value",dia);
	$(".i_hide").eq(1).attr("value",mes);
	$(".i_hide").eq(2).attr("value",any);
	$(".calendari_article .lliure").removeClass("seleccionat");
	$(this).addClass("seleccionat");
}

function moveMonth(){
	if($(this).is(".prev") && mes_visible>0)
		mes_visible--;
	else if($(this).is(".next") && mes_visible<3)
		mes_visible++;
	$("table").hide();
	$("table").eq(mes_visible).show();
	disableButton();
}

function disableButton(){
	if(mes_visible==0)
  		$(".prev").addClass("disabled");
  	else if(mes_visible==3)
  		$(".next").addClass("disabled");
  	else{
  		$(".prev").removeClass("disabled");
  		$(".next").removeClass("disabled");
  	}
}