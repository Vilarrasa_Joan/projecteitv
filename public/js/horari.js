$(document).ready(inicializarEventos);

function inicializarEventos(){
	$(".i_hide").hide();
	$(".lliure").click(clickHora);
	$(".form").submit(function(event){
  	if($(".i_hide").val()==""){
  		alert("Selecciona una hora");
  		event.preventDefault();
  	}
  });
}

function clickHora(){
	var hora=$(this).text();
	$(".i_hide").attr("value",hora);
	$(".horari_article .lliure").removeClass("seleccionat");
	$(this).addClass("seleccionat");
}