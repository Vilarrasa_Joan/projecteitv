<!DOCTYPE html>
<?php 
	session_start();
	$_SESSION['mod'] = 'eliminar';
?>
<html lang="ca">
	<head>
		<?php include("metadata.php") ?>
		<script type="text/javascript" src="../../privat/llibreries/libreria_jquery.js"></script>
	</head>
	<body>
		<?php include("header.php") ?>
	  <div class="container">
	  	<h1 class="site-title">Confirmació</h1>
	    <div class="row">
	      <div class="column">
	      <article>
	      	<h2>Dades:</h2>
          	<table>
	            <tr>
	            <td class="propiedad">Hora: </td><td class="valor"><?php echo $_SESSION['hora']; ?></td>
	            </tr>
	            <tr>
	            <td class="propiedad">Nombre: </td><td class="valor"><?php echo $_SESSION['nom']; ?></td>
	            </tr>
	            <tr>
	            <td class="propiedad">Matrícula: </td><td class="valor"><?php echo $_SESSION['matricula']; ?></td>
	            </tr>
	            <tr>
	            <td class="propiedad">Email: </td><td class="valor"><?php echo $_SESSION['email']; ?></td>
	            </tr>
	            <tr>
	            <td class="propiedad">Telèfon: </td><td class="valor"><?php echo $_SESSION['tlf']; ?></td>
	            </tr>
	            <tr><td colspan="2" style="text-align: center;"><br>Estàs segur de voler esborrar la seva reserva?</td></tr>
	          </table>
          	<div class="row">
	            <div class="column"></div>
	            <div class="column">
	            	<a class="button" href="DadesContraBD.php">Eliminar</a>
	        	</div>
            <div class="column"></div>
          </div>
          </article>
	      </div>
	    </div>
	  </div>
	  <?php include("footer.php") ?>
	</body>
</html>