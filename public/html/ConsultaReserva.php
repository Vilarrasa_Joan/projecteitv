<!DOCTYPE html>
<?php session_start();?>
<html lang="ca">
	<head>
		<?php include("metadata.php") ?>
	</head>
	<body>
		<?php include("header.php") ?>
	  <div class="container">
	  	<h1 class="site-title">Informació de la cita reservada</h1>
	    <div class="row">
	      <div class="column">
	      <article class="ConsultaReserva">
	      	<h2>Dades:</h2>
          	<table>
	            <tr>
	            <td class="propiedad">Hora: </td><td class="valor"><?php echo $_SESSION['hora']; ?></td>
	            </tr>
	            <tr>
	            <td class="propiedad">Nombre: </td><td class="valor"><?php echo $_SESSION['nom']; ?></td>
	            </tr>
	            <tr>
	            <td class="propiedad">Matrícula: </td><td class="valor"><?php echo $_SESSION['matricula']; ?></td>
	            </tr>
	            <tr>
	            <td class="propiedad">Email: </td><td class="valor"><?php echo $_SESSION['email']; ?></td>
	            </tr>
	            <tr>
	            <td class="propiedad">Telèfon: </td><td class="valor"><?php echo $_SESSION['tlf']; ?></td>
	            </tr>
	          </table>
          	<div class="row">
	            <div class="column"></div>
	            <div class="column"><a class="button" href="Esborrar.php">Esborrar Cita</a></div>
	            <div class="column"><a class="button" href="formulari.php">Modificar Dades</a></div>
	            <div class="column"><a class="button" href="CrearCalendari.php">Modificar Data</a></div>
	            <div class="column"><a class="button" href="index.php">Inici</a></div>
            <div class="column"></div>
          </div>
          </article>
	      </div>
	    </div>
	  </div>
	  <?php include("footer.php") ?>
	</body>
</html>