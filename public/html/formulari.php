<!DOCTYPE html>
<?php
  session_start();
  include ('../../privat/llibreries/controlDades.php');
  include ('../../privat/BD/funcionsBD.php');
  //Comprovem que la data i hora enviades siguin de format correcte i ho entrem a una variable de sessió.
  if($_POST)
  {
    $hora = $_POST['hora'] . ':00';
    if(comprovaHora($hora)) $_SESSION['hora'] = $_SESSION['hora'] . ' ' .  $hora;
    else {header("Location:Error.php"); exit();}
  }
?>
<html lang="ca">
<head>
  <?php include("metadata.php") ?>
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script language="javascript" type="text/javascript" src="../js/formulari.js"></script>
</head>
<body>
  <?php include("header.php") ?>
  <div class="container">
    <h1 class="site-title">Cita prèvia ITV</h1>
    <div class="row">
      <div class="column">
        <article class="form_article">
          <h2>Introdueix les teves dades</h2>
          <form class="form nform" action="Confirmacion.php" method="post">
            <input class="insertar" title="Matrícula" type="text" <?php echo "value='" . $_SESSION['matricula'] . "'"; ?> disabled>
            <input class="insertar" title="Nom i Cognoms" type="text" name="nom" placeholder="Nom i Cognoms" <?php if(!is_null($_SESSION['nom'])) echo "value='" . $_SESSION['nom'] . "'"; ?> autofocus>
            <input class="insertar" title="mail@exemple.com" type="email" name="email" placeholder="Email" <?php if(!is_null($_SESSION['email'])) echo "value='" . $_SESSION['email'] . "'"; ?> >
            <input class="insertar" title="Telèfon" type="tel" pattern="^[0-9]{9}$" name="telefon" placeholder="Telèfon" <?php if(!is_null($_SESSION['tlf'])) echo "value='" . $_SESSION['tlf'] . "'"; ?> >
            <input class="button f_button" title="Enviar les teves dades" type="submit" value="Enviar" href="#">
          </form>
        </article>
      </div>
    </div>
  </div>
  <?php include("footer.php") ?>
  </body>
</html>