<?php
	//Script per conectar-se a la BD.
	include ('../../privat/llibreries/controlDades.php');
	include ('../../privat/BD/funcionsBD.php'); 
	session_start();
	//Fem la última comprovació de dades abans de enviar res a la BD.
	if (!comprovaDia(str_replace('-', '',(substr($_SESSION['hora'], 0, 10)))) || 
		!comprovaHora(substr($_SESSION['hora'], -8, 8)) ||
		!comprovaMat($_SESSION['matricula']) || 
		!comprovaTlf($_SESSION['tlf']) || 
		!comprovaNom($_SESSION['nom']) ||  
		!comprovaEmail($_SESSION['email'])) 
	{
		header("Location:Error.php");
		exit();
	}
	//Si està creant una reserva nova...
	if(!$_SESSION['mod'])
	{
		//Comprovar que no tingui cap reserva activa en el futur.
		if(te_reserva($_SESSION['matricula']))
		{
			header("Location:Error.php");
			exit();
		}
		//Comprovar si el vehicle esta registrat a la BD.
		if(!vehicle_registrat($_SESSION['matricula'])) 
		{
			//En cas de no estar registrat el registrem.
			if(!insertar_vehicle($_SESSION['matricula'], $_SESSION['nom'], $_SESSION['tlf'], $_SESSION['email']))
			{
				header("Location:Error.php");
				exit();
			}
		}
		else
		{
			//En cas d'estar registrat l'actualitzaem.
			if(!actualitzar_vehicle($_SESSION['matricula'], $_SESSION['nom'], $_SESSION['tlf'], $_SESSION['email']))
			{
				header("Location:Error.php");
				exit();
			}
		}
		//Insertem la nova reserva.
		if(!insertar_reserva($_SESSION['matricula'], $_SESSION['hora']))
		{
			header("Location:Error.php");
			exit();
		}
		header("Location:Ok.php");
		exit();	
	}
	//Si està modificant una reserva activa...
	elseif($_SESSION['mod']=='modificar')
	{
		$reserva_anterior = extreu_dades_reserva($_SESSION['matricula']);
		//Actualitzem les dades del vehicle.
		if(!actualitzar_vehicle($_SESSION['matricula'], $_SESSION['nom'], $_SESSION['tlf'], $_SESSION['email']))
		{
			header("Location:Error.php");
			exit();
		}
		//Mirem si l'hora de la reserva ha canviat
		if(strcmp($reserva_anterior[0][3], $_SESSION['hora'])!=0)
		{	
			//Actualitzem l'hora de la reserva.
			if(!actualitzar_reserva($_SESSION['matricula'], $_SESSION['hora']))
			{
				header("Location:Error.php");
				exit();
			}
		}
		header("Location:Ok.php");
		exit();
	}
	//Si vol esborrar la seva reserva...
	else
	{	
		//Esborrem la reserva
		if(!esborrar_reserva($_SESSION['matricula']))
		{
			header("Location:Error.php");
			exit();
		}
		header("Location:Ok.php");
		exit();	
	}
?>