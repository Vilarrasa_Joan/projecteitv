<!DOCTYPE html>
<?php session_start();?>
<html lang="ca">
	<head>
		<?php include("metadata.php") ?>
		<script type="text/javascript" src="../../privat/llibreries/libreria_jquery.js"></script>
		<script type="text/javascript" src="Calendario.js"></script>
	</head>
	<body>
		<?php include("header.php") ?>
	  <div class="container">
	    <div class="row">
	      <div class="column">
	      <article>
          	<h2>
          		<?php 
	            	if($_SESSION['mod']=='modificar') echo "Dades Modificades";
	            	elseif ($_SESSION['mod']=='eliminar') echo "Reserva Elimiada";
	            	else echo "Reserva Confirmada"; 
	            ?>	
	        </h2>
          	<p>
          		<?php 
	            	if($_SESSION['mod']=='modificar') echo "Les dades de la cita han estat modificades correctament.<br>";
	            	if($_SESSION['mod']!='eliminar')echo "La seva cita és a les " . substr($_SESSION['hora'], -8, 5) . " del dia " . substr($_SESSION['hora'], 0, 10) . ".<br>Gràcies per comptar amb nosaltres."; 
	            ?>
          	<div class="row">
            	<div class="column"></div>
            	<div class="column">
            		<a class="button" href="index.php">Inici</a>
           		 </div>
           		 <div class="column"></div>
          	</div>
          </article>
	      </div>
	    </div>
	  </div>
	  <?php include("footer.php") ?>
	</body>
</html>