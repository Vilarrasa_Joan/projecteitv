<!DOCTYPE html>
<?php session_start();?>
<html>
	<head>
		<?php include("metadata.php") ?>
	</head>
	<body>
		<?php include("header.php") ?>
	  <div class="container">
	  	<h1 class="site-title">Contacte</h1>
	    <div class="row">
	      <div class="column">
	   		<article class="Contacte">
	   			<p> El taller roman obert de Dilluns a Dissabte de 08:00 a 20:00 hores.<br>
				Els seus telèfons són: 932 033 332 i 932 033 642<br>
				Fax: 932 046 212<br>
				L’adreça electrònica és mam@iam.cat<br>
				La seva ubicació és a l’avinguda d’Esplugues, 38-42<br>
				</p>
				<div class="mapa">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2993.447315607813!2d2.1037992662079543!3d41.38609108963211!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a499ab4ece03bd%3A0x8fa8ffc75d7d95a!2sAv.+d&#39;Esplugues%2C+08034+Barcelona!5e0!3m2!1sca!2ses!4v1513762623892" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</article>
	      </div>
	    </div>
	  </div>
	  <?php include("footer.php") ?>
	</body>
</html>	