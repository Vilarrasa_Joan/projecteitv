<!DOCTYPE html>
<?php
  //scrip per assegurar-se de que la sessió queda eliminada.
  session_start();
  session_unset();
  session_destroy();
  session_write_close();
  setcookie(session_name(),'',0,'/');
  session_regenerate_id(true);
?>
<?php session_start(); ?>
<html lang="ca">
<head>
  <?php include("metadata.php") ?>
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script language="javascript" type="text/javascript" src="../js/index.js"></script>
</head>
<body>
  <?php include("header.php") ?>
  <div class="container">
    <h1 class="site-title">Cita prèvia ITV</h1>
    <div class="row">
      <div class="column">
        <article class="index_article">
          <h2>Demanar una cita ITV</h2>
          <form class="form iform" action="matCheck.php" method="post">
            <input class="insertar" title="Matrícula" type="text" pattern="^[0-9]{4}[b-df-hj-npr-tv-zB-DF-HJ-NPR-TV-Z]{3}$" name="matricula" placeholder="Matrícula">
            <input class="button i_button" title="Enviar matrícula" type="submit" value="Enviar">
          </form>
        </article>
      </div>
    </div>
  </div>
  <?php include("footer.php") ?>
  </body>
</html>