<!DOCTYPE html>
<?php
	session_start();
	include ('../../privat/llibreries/funcions.php');
	include ('../../privat/BD/funcionsBD.php');
	$mesos = ['Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre', 'Octubre', 'Novembre', 'Desembre'];
	$trobat = false;
	$i=0;
	while(!$trobat && $i<12)
	{
		if(strcmp($mesos[$i], $_POST['num_mes'])==0)
		{
			$mes = ($i+1);
		}
		$i++;
	}
	//Comprovem que l'any mes i dia enviats tenen el format correcte per poder imprimir l'horari.
	$mes = testInput($mes);
	$any = testInput($_POST['num_any']);
	$dia = testInput($_POST['num_dia']);
	if($dia<10) $dia = '0' . $dia;
	if($mes<10) $mes = '0' . $mes;
	$data = $any . $mes . $dia;
	$data = substr_replace($data, '-', 4, 0);
	$data = substr_replace($data, '-', 7, 0);
	$_SESSION['hora'] = $data;
	//Omplim un array amb les hores del dia que estan ocupades.
	$horesOcupades = llista_hores_ocupades($any, $mes, $dia);
	$horari = imprimeixHorari($horesOcupades);
?>

<html lang="ca">
	<head>
		<?php include("metadata.php") ?>
		<script type="text/javascript" src="../../privat/llibreries/libreria_jquery.js"></script>
		<script type="text/javascript" src="../js/horari.js"></script>
		<script type="text/javascript" src="../js/jquery.js"></script>
		<title>Motors Ausiàs March</title>
	</head>
	<body>
		<?php include("header.php") ?>
	  <div class="container">
	    <h1 class="site-title">Seleccioni l'hora que desitgi: </h1>
	    <div class="row">
	      <div class="column">
	        <article class="horari_article">
	        	<h2><?php echo $dia . " de " . $mesos[intval($mes-1)] . " del " . $any ?></h2>
	          <?php echo $horari ?>
	          <form class="form" action="formulari.php" method="post">
	      			<input class="i_hide" type="text" name="hora">    		
		      		<input class="button c_button" type="submit" value="Confirmar">
	      	  </form>
	      	  <a class="button" href="CrearCalendari.php">Tornar enrere</a>
	        </article>
	      </div>
	    </div>
	  </div>
	  <?php include("footer.php") ?>
	</body>
</html>