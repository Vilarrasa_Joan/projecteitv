<!DOCTYPE html>
<?php 
	session_start();
	include ("../../privat/llibreries/controlDades.php");
	if ($_POST){
		if(comprovaNom($_POST['nom'])) $_SESSION['nom'] = $_POST['nom'];
		else {header("Location:ErrorMatricula.php"); exit();}
		if(comprovaEmail($_POST['email'])) $_SESSION['email'] = $_POST['email'];
		else {header("Location:ErrorMatricula.php"); exit();}
		if(comprovaTlf($_POST['telefon'])) $_SESSION['tlf'] = $_POST['telefon'];
		else {header("Location:ErrorMatricula.php"); exit();}
	} 
?>
<html lang="ca">
	<head>
		<?php include("metadata.php") ?>
		<script type="text/javascript" src="../../privat/llibreries/libreria_jquery.js"></script>
	</head>
	<body>
		<?php include("header.php") ?>
	  <div class="container">
	  	<h1 class="site-title">Confirmació</h1>
	    <div class="row">
	      <div class="column">
	      <article>
	      	<h2>Dades:</h2>
          	<table>
	            <tr>
	            <td class="propiedad">Hora: </td><td class="valor"><?php echo $_SESSION['hora']; ?></td>
	            </tr>
	            <tr>
	            <td class="propiedad">Nombre: </td><td class="valor"><?php echo $_SESSION['nom']; ?></td>
	            </tr>
	            <tr>
	            <td class="propiedad">Matrícula: </td><td class="valor"><?php echo $_SESSION['matricula']; ?></td>
	            </tr>
	            <tr>
	            <td class="propiedad">Email: </td><td class="valor"><?php echo $_SESSION['email']; ?></td>
	            </tr>
	            <tr>
	            <td class="propiedad">Telèfon: </td><td class="valor"><?php echo $_SESSION['tlf']; ?></td>
	            </tr>
	          </table>
          	<div class="row">
	            <div class="column"></div>
	            <div class="column"><a class="button" href="formulari.php">Modificar Dades</a></div>
	            <div class="column"><a class="button" href="CrearCalendari.php">Modificar Data</a></div>
	            <div class="column">
	            	<a class="button" href="DadesContraBD.php">
	            		<?php 
	            			//Segons si és un modificació o una reserva nova el missatge varia.
	            			if($_SESSION['mod']=='modificar') echo "Confirmar Canvis";
	            			else echo "Confirmar Reserva"; 
	            		?>	            
	            	</a>
	        	</div>
            <div class="column"></div>
          </div>
          </article>
	      </div>
	    </div>
	  </div>
	  <?php include("footer.php") ?>
	</body>
</html>