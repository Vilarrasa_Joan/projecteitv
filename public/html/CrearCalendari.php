<!DOCTYPE html>
<?php
	include ('../../privat/llibreries/funcions.php');
	include ('../../privat/BD/funcionsBD.php');
	//Afegirem a calendar el contingut de 4 mesos segons la BD.
	$calendar='';
	for($i=0;$i<4;$i++)
	{
		//Si el mes es passa de 12 sumem 1 any i restem 12 als mesos per tal que tornin a començar a 1.
		if ((date('n')+$i)>12){
			$mes = intval(date('n')+$i)-12;
			$any = intval(date('Y'))+1;
		}
		else{
			$mes = date('n')+$i;
			$any = date('Y');
		}
		//Calculem els dies del mes i la posició a la setmana del primer dia (de 0(Diumenge) a 6(Dissabte))
		$diesMes = diesMes($mes, $any);
		$primerDia = date('N', (strtotime($any . "-" . $mes . "-01")));
		//Modificar la posició del primer dia a la setmana per tal de que quedi de 0(Dilluns) a 6(Diumenge)
		if($primerDia==0) $primerDia=6;
		else $primerDia--;
		if($mes<10) $mes = '0' . $mes;
		//omplim una array de dies ocupats del mes concret.
		$diesOcupats = llista_dies_ocupats($any, $mes);
		$calendar = $calendar . imprimeixCalendari($primerDia, $diesMes, $diesOcupats, $mes, $any);
	}
?>
<html lang="ca">
	<head>
		<?php include("metadata.php"); ?>
		<script type="text/javascript" src="../js/jquery.js"></script>
		<script type="text/javascript" src="../js/Calendari.js"></script>
	</head>
	<body>
		<?php include("header.php"); ?>
	  <div class="container">
	    <h1 class="site-title">Seleccioni el dia:</h1>
	    <div class="row">
	      <div class="column">
	      	<article class="calendari_article">
	      		<div>
		      		<a class="button prev"><img src="../img/ic_navigate_before_white_24px.svg"></img></a>
		      		<a class="button next"><img src="../img/ic_navigate_next_white_24px.svg"></img></a>
		      		<?php echo $calendar; ?>
		      	</div>
	      		<form class="form" action="CrearHorari.php" method="post">
	      			<input class="i_hide" type="number" name="num_dia">
	      			<input class="i_hide" type="text" name="num_mes">
	      			<input class="i_hide" type="number" name="num_any">	      		
		      		<input class="button c_button" type="submit" value="Confirmar">
	      		</form>
	      	</article>
	      </div>
	    </div>
	  </div>
	  <?php include("footer.php"); ?>
	</body>
</html>