<?php
	include ('../../privat/BD/funcionsBD.php');
	include ("../../privat/llibreries/controlDades.php");
	session_start();
	//Passem de minúscules a majúscules el passem per la funció testInput.
	$matricula = arreglaMat($_POST['matricula']);
	//Si la matrícula no té el format correcte o el mètode de enviament no és post no pot continuar
	if (!$_POST || !comprovaMat($matricula)){
		header("Location:ErrorMatricula.php");
		exit();
	}
	else
	{
		//Assignem la matrícula a la sessió
		$_SESSION['matricula'] = $matricula;
		//si el vehicle està registrat a la DB n'agafem les dades
		$dades = dades_vehicle_registrat($matricula);
		if($dades)
		{
			$_SESSION['nom'] = $dades[0][1];
			$_SESSION['tlf'] = $dades[0][2];
			$_SESSION['email'] = $dades[0][3];
		}
		//si té una reserva activa n'agafem les dades de la reserva i l'enviem a la pàgina de consulta
		if(te_reserva($matricula))
		{
			$infoReserva = extreu_dades_reserva($matricula);
			//Inicialitzem $_SESSION['mod'] que ens permetrà distingir entre una modificació i una reserva nova
			$_SESSION['mod'] = 'modificar';
			$_SESSION['hora'] = $infoReserva[0][3];
			header("Location:ConsultaReserva.php");
			exit();
		}
		//Enviem a l'usuari al calendari del mes.
		else{
			header("Location:CrearCalendari.php");
			exit();
		}
	}
?>