<!DOCTYPE html>
<?php session_start(); ?>
<html lang="ca">
	<head>
		<?php include("metadata.php") ?>
		<script type="text/javascript" src="../../privat/llibreries/libreria_jquery.js"></script>
	</head>
	<body>
		<?php include("header.php") ?>
	  <div class="container">
	    <div class="row">
	      <div class="column">
	      <article>
          	<h2>Error</h2>
          	<p>La seva cita no s'ha pogut reservar correctament.<br>
          	Torni a provar-ho.</p>
          	<div class="row">
            	<div class="column"></div>
            	<div class="column">
            		<a class="button" href="index.php">Inici</a>
           		 </div>
           		 <div class="column"></div>
          	</div>
          </article>
	      </div>
	    </div>
	  </div>
	  <?php include("footer.php") ?>
	</body>
</html>